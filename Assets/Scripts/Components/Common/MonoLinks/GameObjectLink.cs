using System;
using UnityEngine;

namespace Components.Common.MonoLinks
{
    [Serializable]
    public struct GameObjectLink
    {
        public GameObject Value;
    }
}
