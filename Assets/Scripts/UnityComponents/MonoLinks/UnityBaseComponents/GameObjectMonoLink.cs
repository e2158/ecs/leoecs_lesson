using Leopotam.Ecs;
using Components.Common.MonoLinks;
using UnityComponents.MonoLinks.Base;

namespace UnityComponents.MonoLinks.UnityBaseComponents
{
    public class GameObjectMonoLink : MonoLink<GameObjectLink>
    {
        public override void Make(ref EcsEntity entity) =>
            entity.Get<GameObjectLink>() = new GameObjectLink
            {
                Value = gameObject
            };

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (Value.Value == null)
                Value = new GameObjectLink()
                {
                    Value = gameObject
                };
        }
#endif
    }
}
