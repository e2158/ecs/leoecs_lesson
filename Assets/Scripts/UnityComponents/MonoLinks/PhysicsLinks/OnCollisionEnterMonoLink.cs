using UnityEngine;
using Leopotam.Ecs;
using Components.PhysicsEvents;
using UnityComponents.MonoLinks.Base;

namespace UnityComponents.MonoLinks.PhysicsLinks
{
    public class OnCollisionEnterMonoLink : PhysicsLinkBase
    {
        public void OnCollisionEnter(Collision collision) =>
            _entity.Get<OnCollisionEnterEvent>() = new OnCollisionEnterEvent
            {
                Collision = collision,
                Sender = gameObject
            };
    }
}
