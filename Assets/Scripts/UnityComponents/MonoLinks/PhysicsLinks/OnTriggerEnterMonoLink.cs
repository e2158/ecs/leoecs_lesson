using UnityEngine;
using Leopotam.Ecs;
using Components.PhysicsEvents;
using UnityComponents.MonoLinks.Base;

namespace UnityComponents.MonoLinks.PhysicsLinks
{
    public class OnTriggerEnterMonoLink : PhysicsLinkBase
    {
        public void OnTriggerEnter(Collider other) =>
            _entity.Get<OnTriggerEnterEvent>() = new OnTriggerEnterEvent
            {
                Collider = other,
                Sender = gameObject
            };
    }
}
