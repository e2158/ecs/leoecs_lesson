using Components.Common.Input;
using Components.Objects.Tags;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems.InputSystems
{
    public class KeyInputSystem : IEcsRunSystem
    {
        private readonly EcsWorld _world = null;
        private readonly EcsFilter<PlayerTag> _filter = null;

        public void Run()
        {
            if (!Input.anyKeyDown)
                return;

            foreach (int index in _filter)
            {
                ref EcsEntity entity = ref _filter.GetEntity(index);
                entity.Get<AnyKeyDownTag>();
            }
        }
    }
}
