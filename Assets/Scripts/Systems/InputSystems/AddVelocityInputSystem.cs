using Leopotam.Ecs;
using UnityComponents.Common;
using Components.Common.Input;
using Components.Objects.Moves;

namespace Systems.InputSystems
{
    public class AddVelocityInputSystem : IEcsRunSystem
    {
        private readonly StaticData _staticData = null;
        private readonly EcsFilter<AnyKeyDownTag> _filter = null;

        public void Run()
        {
            foreach (int index in _filter)
            {
                ref EcsEntity entity = ref _filter.GetEntity(index);
                ref Velocity velocity = ref entity.Get<Velocity>();
                velocity.Value += _staticData.PlayerAddForce;
            }
        }
    }
}
