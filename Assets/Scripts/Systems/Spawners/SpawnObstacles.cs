using Components.Common;
using Leopotam.Ecs;
using UnityComponents.Common;
using UnityEngine;

namespace Systems.Spawners
{
    public class SpawnObstacles : IEcsInitSystem, IEcsRunSystem
    {
        private readonly StaticData _staticData = null;
        private readonly SceneData _sceneData = null;
        private readonly EcsWorld _world = null;

        private float _spawnDelay;
        private float _lastTime;

        public void Init()
        {
            _spawnDelay = _staticData.SpawnTimer;
        }

        public void Run()
        {
            _lastTime += Time.deltaTime;

            if (!(_lastTime > _spawnDelay))
                return;

            _world.NewEntity().Get<SpawnPrefab>() = new SpawnPrefab
            {
                Prefab = _staticData.ObstaclePrefab,
                Position = _sceneData.SpawnObstaclePosition.position,
                Rotation = Quaternion.identity,
                Parent = _sceneData.SpawnObstaclePosition
            };

            _lastTime -= _spawnDelay;
        }
    }
}
