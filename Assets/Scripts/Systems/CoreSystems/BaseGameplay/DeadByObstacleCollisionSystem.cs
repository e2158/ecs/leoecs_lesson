using Leopotam.Ecs;
using Components.Core;
using Components.GameStates.GameplayEvents;

namespace Systems.CoreSystems.BaseGameplay
{
    public class DeadByObstacleCollisionSystem : IEcsRunSystem
    {
        private readonly EcsWorld _world = null;
        private readonly EcsFilter<OnObstacleCollisionEvent> _filter = null;

        public void Run()
        {
            if (_filter.IsEmpty())
                return;

            _world.NewEntity().Get<DeadEvent>();
        }
    }
}
