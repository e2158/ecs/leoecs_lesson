using Components.Core;
using Leopotam.Ecs;
using Services;
using UnityComponents.Common;

namespace Systems.CoreSystems.BaseGameplay
{
	public class ScoreCounterSystem : IEcsRunSystem
	{
		private readonly SceneData _sceneData = null;
		private readonly ScoreService _score = null;
		private readonly EcsWorld _world = null;
		private readonly EcsFilter<OnObstacleExit> _obstacleScoreFilter = null;

		public void Run()
		{
			if (_obstacleScoreFilter.IsEmpty())
				return;

			foreach (int index in _obstacleScoreFilter)
			{
				OnObstacleExit obstacleScore = _obstacleScoreFilter.Get1(index);
				_score.AddScore(obstacleScore.Score);
				_sceneData.GameHud.SetScore(_score.Score);
			}
		}
	}
}