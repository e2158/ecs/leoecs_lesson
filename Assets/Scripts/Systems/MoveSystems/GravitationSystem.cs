using UnityEngine;
using Leopotam.Ecs;
using UnityComponents.Common;
using Components.Objects.Moves;

namespace Systems.MoveSystems
{
    public class GravitationSystem : IEcsRunSystem
    {
        private readonly StaticData _staticData;
        private readonly EcsFilter<Gravitational> _filter = null;

        public void Run()
        {
            float deltaTime = Time.fixedTime;
            foreach (int index in _filter)
            {
                ref EcsEntity entity = ref _filter.GetEntity(index);
                ref Velocity velocity = ref entity.Get<Velocity>();

                velocity.Value += _staticData.GlobalGravitation * deltaTime;
            }
        }
    }
}
